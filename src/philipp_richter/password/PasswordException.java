/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.password;

/**
 *
 * @author Philipp Richter
 */
public class PasswordException extends IllegalArgumentException{
    protected final PasswordEntity _passwordEntity;
    
    public PasswordException(PasswordEntity pe,String message){
        super(message);
        _passwordEntity=pe;
    }
    
    public PasswordEntity getPasswordEntity(){
        return _passwordEntity;
    }
}
