package philipp_richter.server;

import java.net.InetAddress;

/**
 *
 * @author Philipp Richter
 */
public class CrackNode {
    protected final InetAddress _ip;
    protected CrackNodeState _crackNodeState;

    public CrackNode(InetAddress ip, CrackNodeState crackNodeState) {
        _ip = ip;
        _crackNodeState = crackNodeState;
    }
    
    public CrackNode(InetAddress ip){
        this(ip,CrackNodeState.UNKNOWN);
    }
    
    public InetAddress getIP(){
        return _ip;
    }
    
    public CrackNodeState getCrackNodeState(){
        return _crackNodeState;
    }
    
    public void setCrackNodeState(CrackNodeState cns){
        _crackNodeState=cns;
    }
    
    public String getPrettyString(){
        return "("+getIP().getHostAddress()+")";
    }

    @Override
    public int hashCode() {
        return getIP().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof CrackNode) && (getIP().equals(((CrackNode)o).getIP())));
    }
    
    
}
