package philipp_richter.client;

import philipp_richter.password.PasswordEntity;

/**
 *
 * @author Philipp Richter
 */
public class ClientRequestTypeFactory {
    
    public ClientRequestType getClientRequestTypeDiscover(){
        return new ClientRequestTypeDiscover();
    }
    
    public ClientRequestType getClientRequestTypePrepare(){
        return new ClientRequestTypePrepare();
    }
    
    public ClientRequestType getClientRequestTypeStart(PasswordEntity pe,CrackMethod cm){
        return new ClientRequestTypeStart(pe,cm);
    }
    
    public ClientRequestType getClientRequestTypeStop(){
        return new ClientRequestTypeStop();
    }
}
