package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeBadclient extends CrackNodeResponseType{
        
    public static final byte[] BADCLIENT_NAME=StringUtils.getBytesUtf8("BADCLIENT");
    
    public CrackNodeResponseTypeBadclient() {
        super(BADCLIENT_NAME);
    }
}
