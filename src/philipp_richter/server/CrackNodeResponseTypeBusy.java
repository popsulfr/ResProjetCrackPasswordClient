package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeBusy extends CrackNodeResponseType{
    
    public static final byte[] BUSY_NAME=StringUtils.getBytesUtf8("BUSY");
    
    public CrackNodeResponseTypeBusy() {
        super(BUSY_NAME);
    }
}
