package philipp_richter.client;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.server.CrackNode;
import philipp_richter.server.Server;

/**
 *
 * @author Philipp Richter
 */
public class Client {
    public static final int CLIENT_CONNECTION_PORT=1664;
    public static final byte[] CLIENT_HEADER=StringUtils.getBytesUtf8("CRACKNODECLIENT");
    public static final byte CLIENT_SEPARATOR='%';
    
    protected int _connectionPort;
    protected int _serverPort;
    
    public Client(int connectionPort,int serverPort){
        _connectionPort=connectionPort;
        _serverPort=serverPort;
    }
    
    public Client(int connectionPort){
        this(connectionPort,Server.SERVER_PORT);
    }
    
    public Client(){
        this(CLIENT_CONNECTION_PORT);
    }
    
    public int getConnectionPort(){
        return _connectionPort;
    }
    
    public int getServerPort(){
        return _serverPort;
    }
    
    public void setServerPort(int port){
        _serverPort=port;
    }
    
    public void setConnectionPort(int port){
        _connectionPort=port;
    }
    
    public void writeClientRequestHeader(OutputStream os) throws IOException{
        os.write(CLIENT_HEADER);
        os.write(CLIENT_SEPARATOR);
        os.write(StringUtils.getBytesUtf8(String.valueOf(getServerPort())));
        os.write(CLIENT_SEPARATOR);
    }
    
    public void writeClientRequest(OutputStream os,ClientRequestType crt) throws IOException{
        writeClientRequestHeader(os);
        os.write(crt.getClientRequestTypeName());
        os.write(CLIENT_SEPARATOR);
        crt.writeClientRequest(os);
    }
    
    public void sendClientRequest(Socket sock,ClientRequestType crt) throws IOException{
        DataOutputStream dos=new DataOutputStream(sock.getOutputStream());
        writeClientRequest(dos, crt);
        dos.flush();
    }
    
    public Socket crackNodeConnector(CrackNode cn) throws IOException{
        Socket s=new Socket(cn.getIP(),getConnectionPort());
        s.setReuseAddress(true);
        s.setSoTimeout(1000);
        return s;
    }
    
    /**
     * 
     * @return
     * @throws SocketException 
     */
    public InetAddress getNetworkBroadcastAddress() throws SocketException{
        Enumeration<NetworkInterface> interfaces=NetworkInterface.getNetworkInterfaces();
        NetworkInterface loopback=null;
        for(;interfaces.hasMoreElements();){
            NetworkInterface ni=interfaces.nextElement();
            if(!ni.isLoopback()){
                for(InterfaceAddress ia:ni.getInterfaceAddresses()){
                    InetAddress broadcast;
                    if((broadcast=ia.getBroadcast())!=null) return broadcast;
                }
            }
        }
        return Inet4Address.getLoopbackAddress();
    }
    
    public boolean broadCast() throws SocketException, IOException{
        DatagramSocket ds=new DatagramSocket(getServerPort());
        ds.setBroadcast(true);
        InetAddress broadcast=getNetworkBroadcastAddress();
        if(broadcast!=null){
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            writeClientRequest(baos, new ClientRequestTypeDiscover());
            DatagramPacket dp=new DatagramPacket(baos.toByteArray(),baos.toByteArray().length,broadcast,getConnectionPort());
            ds.send(dp);
            ds.close();
            return true;
        }
        return false;
    }
}
