/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.client.Alphabet;

/**
 *
 * @author Philipp Richter
 */
public class BruteForce extends CrackMethod{
    public static final int DEFAULT_MIN_WORD_SIZE=1;
    public static final int DEFAULT_MAX_WORD_SIZE=64;
    
    public static final String BRUTEFORCE_NAME="BRUTEFORCE";
    
    protected int _minWordSize;
    protected int _maxWordSize;
    protected Alphabet _alphabet;
    
    protected BigInteger _maxcombinations=BigInteger.ZERO;
    protected BigInteger _currentPos=BigInteger.ZERO;
    
    public BruteForce(Alphabet alphabet,int minWordSize,int maxWordSize){
        super(BRUTEFORCE_NAME);
        setAlphabet(alphabet);
        setMinWordSize(minWordSize);
        setMaxWordSize(maxWordSize);
    }
    
    public BruteForce(Alphabet alphabet){
        this(alphabet,DEFAULT_MIN_WORD_SIZE,DEFAULT_MAX_WORD_SIZE);
    }
    
    public int getMinWordSize(){
        return _minWordSize;
    }
    
    public int getMaxWordSize(){
        return _maxWordSize;
    }
    
    public Alphabet getAlphabet(){
        return _alphabet;
    }
    
    public void setMinWordSize(int minWordSize){
        assert minWordSize >=0;
        _minWordSize=minWordSize;
    }
    
    public void setMaxWordSize(int maxWordSize){
        assert maxWordSize>0;
        _maxWordSize=maxWordSize;
    }
    
    public void setAlphabet(Alphabet alphabet){
        _alphabet=alphabet;
    }

    private void calculateNextChunk(){
        _maxcombinations=getMaxCombinations();
        BigInteger diff=_maxcombinations.subtract(_currentPos);
        if(diff.compareTo(BigInteger.valueOf(CHUNK_SIZE))>0){
            _currentPos=_currentPos.add(BigInteger.valueOf(CHUNK_SIZE));
        }
        else _currentPos=_maxcombinations;
    }
    
    private int chunkSize(){
        BigInteger diff=_maxcombinations.subtract(_currentPos);
        if(diff.compareTo(BigInteger.valueOf(CHUNK_SIZE))>0){
            return CHUNK_SIZE;
        }
        else return diff.intValue();
    }
    
    @Override
    public CrackMethod getNextChunk() {
        BruteForce bf=new BruteForce(getAlphabet(),getMinWordSize(),getMaxWordSize());
        bf._currentPos=_currentPos;
        bf.calculateNextChunk();
        if(bf._currentPos.compareTo(bf._maxcombinations)<0){
            System.out.println("nextchunk: "+bf._currentPos.toString(10)+" of "+bf._maxcombinations.toString(10));
            return bf;
        }
        else return null;
    }
    
    @Override
    public float getPercentage(){
        BigDecimal max=new BigDecimal(_maxcombinations);
        BigDecimal curr=new BigDecimal(_currentPos);
        BigDecimal oh=new BigDecimal(100);
        BigDecimal res=curr.divide(max,4,RoundingMode.HALF_UP).multiply(oh);
        return res.floatValue();
    }

    @Override
    public void writeClientRequest(OutputStream os) throws IOException {
        if(_maxcombinations.compareTo(BigInteger.ZERO)==0){
            _maxcombinations=getMaxCombinations();
        }
        os.write(StringUtils.getBytesUtf8(getAlphabet().getAlphabet()));
        os.write(Client.CLIENT_SEPARATOR);
        os.write(StringUtils.getBytesUtf8(String.valueOf(getMaxWordSize())));
        os.write(Client.CLIENT_SEPARATOR);
        os.write(StringUtils.getBytesUtf8(getWordAtPos(_currentPos)));
        os.write(Client.CLIENT_SEPARATOR);
        os.write(StringUtils.getBytesUtf8(String.valueOf(chunkSize())));
        os.write(Client.CLIENT_SEPARATOR);
    }
    
    public BigInteger getMaxCombinations(){
        BigInteger maxcomb=BigInteger.ZERO;
        for(int i=getMinWordSize();i<=getMaxWordSize();++i){
            maxcomb=maxcomb.add(BigInteger.valueOf(getAlphabet().getSize()).pow(i));
        }
        return maxcomb;
    }
    
    public String getWordAtPos(BigInteger pos){
        int alphlen=getAlphabet().getSize();
        int i=getMinWordSize();
        int begelws=1;
        BigInteger maxcombinations=BigInteger.ZERO;
        BigInteger begelwsnum=BigInteger.ZERO;
        for(;i<=getMaxWordSize();++i){
            BigInteger power=BigInteger.valueOf(alphlen).pow(i);
            BigInteger newmaxcomb=maxcombinations.add(power);
            if(pos.compareTo(maxcombinations)>=0 && pos.compareTo(newmaxcomb)<0){
                begelws=i;
                begelwsnum=pos.subtract(maxcombinations);
            }
            maxcombinations=newmaxcomb;
        }
        StringBuilder word=new StringBuilder();
        BigInteger begelwsnmod=begelwsnum.add(BigInteger.ONE);
        for(i=0;i<begelws;++i){
            BigInteger power=BigInteger.valueOf(alphlen).pow(begelws-(i+1));
            BigInteger index=begelwsnum.mod(begelwsnmod).divide(power);
            word.append(getAlphabet().getAlphabet().charAt(index.intValue()));
            begelwsnmod=power;
        }
        return word.toString();
    }
}
