package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeNotrunning extends CrackNodeResponseType{
        
    public static final byte[]NOTRUNNING_NAME=StringUtils.getBytesUtf8("NOTRUNNING");
    
    public CrackNodeResponseTypeNotrunning() {
        super(NOTRUNNING_NAME);
    }
}
