package philipp_richter.server;

/**
 *
 * @author Philipp Richter
 */
public enum CrackNodeState {
    WAITING,RESERVED,BUSY,UNKNOWN;
}
