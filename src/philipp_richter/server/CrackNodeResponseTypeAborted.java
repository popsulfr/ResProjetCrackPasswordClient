package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeAborted extends CrackNodeResponseType{
        
    public static final byte[] ABORTED_NAME=StringUtils.getBytesUtf8("ABORTED");
    
    public CrackNodeResponseTypeAborted() {
        super(ABORTED_NAME);
    }
}
