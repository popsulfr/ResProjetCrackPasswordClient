/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.password;

import philipp_richter.password.HashDoesNotMatchPasswordException;

/**
 * Supposed to represent informations surrounding a password
 * 
 * @author Philipp Richter
 */
public class PasswordEntity {
    protected String _passwordHash=null;
    protected String _salt=null;
    protected PasswordHashType _passwordHashType=null;
    protected String _password=null;
    
    public PasswordEntity(){}

    /**
     * 
     * @param passwordHash
     * @param passwordHashType
     * @param salt 
     */
    public PasswordEntity(String passwordHash,PasswordHashType passwordHashType,String salt){
        _passwordHash=passwordHash;
        setPasswordHashType(passwordHashType);
        setSalt(salt);
    }
    
    public static PasswordEntity createPasswordEntityFromCryptHashString(String cryptHashString){
        PasswordHashType pht=PasswordHashType.createFromHashString(cryptHashString);
        return new PasswordEntity(pht.extractHash(cryptHashString),pht, pht.extractSalt(cryptHashString));
    }
    
    public String getPasswordHash(){return _passwordHash;}
    public String getSalt(){return _salt;}
    public PasswordHashType getPasswordHashType(){return _passwordHashType;}
    public String getPassword(){return _password;}
    
    private String getCryptCompatibleSaltParameter(String salt){
        if(getPasswordHashType()==null) return null;
        String ccsp="";
        if(getPasswordHashType()!=PasswordHashType.DES){
            ccsp+="$"+getPasswordHashType().getPasswordHashCode()+"$";
            if(salt!=null) ccsp+=salt+"$";
            else ccsp+="$";
        }
        else{
            if(salt!=null) ccsp+=salt;
        }
        return ccsp;
    }
    
    public String getCryptCompatibleSaltParameter(){
        return getCryptCompatibleSaltParameter(getSalt());
    }
    
    private String getCryptCompatibleHashString(String passwordHash,String salt){
        if(passwordHash==null || passwordHash.isEmpty()) return null;
        return (getCryptCompatibleSaltParameter(salt)!=null)?getCryptCompatibleSaltParameter(salt)+passwordHash:passwordHash;
    }
    public String getCryptCompatibleHashString(){
        return getCryptCompatibleHashString(getPasswordHash(),getSalt());
    }

    /**
     * 
     * @param passwordHash
     * @throws HashDoesNotMatchPasswordException when hash and password do not match up
     */
    public void setPasswordHash(String passwordHash) throws HashDoesNotMatchPasswordException{
        if(getPassword()!=null && !getPassword().isEmpty() && !passwordHashMatches(passwordHash)){
            throw new HashDoesNotMatchPasswordException(this, passwordHash);
        }
        _passwordHash=passwordHash;
    }
    
    /**
     * 
     * @param salt
     * @throws SaltDoesNotMatchHashPasswordException 
     */
    public void setSalt(String salt) throws SaltDoesNotMatchHashPasswordException{
        if(getPassword()!=null && !getPassword().isEmpty()){
                if(getPasswordHash()!=null && !getPasswordHash().isEmpty()){
                    if(!saltMatches(salt)) throw new SaltDoesNotMatchHashPasswordException(this,salt);
                }
                else{
                    generateAndStorePasswordHash(getPassword(),getPasswordHashType(),salt);
                }
        }
        _salt=salt;
    }
    
    private void generateAndStorePasswordHash(String password,PasswordHashType pht,String salt){
        if(pht!=null && pht!=PasswordHashType.UNKNOWN_HASH_TYPE){
            _passwordHash=(salt!=null && !salt.isEmpty())?
                    pht.extractHash(pht.encrypt(password, salt)):
                    pht.extractHash(pht.encrypt(password, null));
        }
    }
    
    /**
     * 
     * @param passwordHashType
     * @throws PasswordHashTypeIsNotCompatibleException 
     */
    public void setPasswordHashType(PasswordHashType passwordHashType) throws PasswordHashTypeIsNotCompatibleException{
        if(getPasswordHash()!=null && !getPasswordHash().isEmpty()){
            if(!passwordHashTypeMatches(passwordHashType)) throw new PasswordHashTypeIsNotCompatibleException(this, passwordHashType);
        }
        else if(getPassword()!=null && !getPassword().isEmpty()){
            generateAndStorePasswordHash(getPassword(),passwordHashType,getSalt());
        }
        _passwordHashType=passwordHashType;
    }
    
    /**
     * 
     * @param password
     * @throws PasswordDoesNotMatchHashException 
     */
    public void setPassword(String password) throws PasswordDoesNotMatchHashException{
        if(getPasswordHash()!=null && !getPasswordHash().isEmpty()){
            if(!passwordMatches(password)) throw new PasswordDoesNotMatchHashException(this, password);
        }
        else{
            generateAndStorePasswordHash(password,getPasswordHashType(),getSalt());
        }
        _password=password;
    }
    
    public boolean saltMatches(String salt){
        return (getPassword()!=null && !getPassword().isEmpty() &&
                getPasswordHash()!=null && !getPasswordHash().isEmpty() &&
                getPasswordHashType().extractHash(getPasswordHashType().encrypt(getPassword(), salt)).equals(getPasswordHash()));
    }
    
    public boolean passwordHashTypeMatches(PasswordHashType pht){
        return getPasswordHash()!=null &&
                !getPasswordHash().isEmpty() &&
                getPasswordHash().length()==pht.getHashLength();
    }
    
    public boolean passwordHashMatches(String hash){
        if(getPassword()!=null){
            return getPasswordHashType().extractHash(getPasswordHashType().encrypt(getPassword(), getSalt())).equals(hash);
        }
        return false;
    }
    
    public boolean passwordMatches(String password){
        if(getPasswordHash()!=null && getPasswordHashType()!=null && getPasswordHashType()!=PasswordHashType.UNKNOWN_HASH_TYPE){
            return getPasswordHashType().extractHash(getPasswordHashType().encrypt(password, getSalt())).equals(getPasswordHash());
        }
        return false;
    }
}
