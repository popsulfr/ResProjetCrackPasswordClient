package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeAccepted extends CrackNodeResponseType{
    
    public static final byte[] ACCEPTED_NAME=StringUtils.getBytesUtf8("ACCEPTED");
    
    public CrackNodeResponseTypeAccepted() {
        super(ACCEPTED_NAME);
    }
}
