/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeAborted;
import philipp_richter.server.CrackNodeResponseTypeBadclient;
import philipp_richter.server.CrackNodeResponseTypeFactory;
import philipp_richter.server.CrackNodeResponseTypeNotrunning;

/**
 *
 * @author Philipp Richter
 */
public class ClientRequestTypeStop extends ClientRequestType{
    public static final byte[] CLIENT_REQUEST_STOP=StringUtils.getBytesUtf8("STOP");

    public ClientRequestTypeStop() {
        super(CLIENT_REQUEST_STOP);
    }

    @Override
    public boolean writeClientRequest(OutputStream os) {
        return true;
    }

    @Override
    public Set<CrackNodeResponseType> getCrackNodeResponseTypeFailure(CrackNodeResponseTypeFactory cnrtf) {
        Set<CrackNodeResponseType> cnrt=new HashSet<>();
        cnrt.add(cnrtf.getCrackNodeResponseTypeNotrunning());
        cnrt.add(cnrtf.getCrackNodeResponseTypeBadclient());
        return cnrt;
    }

    @Override
    public CrackNodeResponseType getCrackNodeResponseTypeSuccess(CrackNodeResponseTypeFactory cnrtf) {
        return cnrtf.getCrackNodeResponseTypeAborted();
    }
    
    
}
