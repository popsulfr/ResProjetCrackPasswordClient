package philipp_richter.password;

/**
 *
 * @author Philipp Richter
 */
public class HashDoesNotMatchPasswordException extends PasswordException{
    protected final String _hash;
    
    public HashDoesNotMatchPasswordException(PasswordEntity pe,String hash) {
        super(pe,"'"+hash+"' does not match password '"+pe.getPassword()+"'!");
        _hash=hash;
    }
    
    public String getHash(){
        return _hash;
    }
}
