package philipp_richter.server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeReady extends CrackNodeResponseType{

    public static final byte[] SIGNAL_NAME=StringUtils.getBytesUtf8("READY");
    
    public CrackNodeResponseTypeReady() {
        super(SIGNAL_NAME);
    }
}
