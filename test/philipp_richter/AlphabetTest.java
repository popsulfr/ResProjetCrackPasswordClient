/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter;

import philipp_richter.client.Alphabet;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Philipp Richter
 */
public class AlphabetTest {

    /**
     * Test of setAlphabet method, of class Alphabet.
     */
    @Test
    public void testSetAlphabet() {
        System.out.println("setAlphabet()");
        String name="Test Alphabet";
        String alphIn="aabbccddeeffgggghijklamnobp";
        String alphOut="abcdefghijklmnop";
        Alphabet aO=new Alphabet(name, alphIn);
        assertEquals(alphOut, aO.getAlphabet());
    }
    
}
