package philipp_richter;

import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.client.Client;
import philipp_richter.client.ClientRequestType;
import philipp_richter.client.ClientRequestTypeFactory;
import philipp_richter.client.CrackMethod;
import philipp_richter.client.NullMethod;
import philipp_richter.password.PasswordEntity;
import philipp_richter.server.CrackNode;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeDone;
import philipp_richter.server.CrackNodeResponseTypeFactory;
import philipp_richter.server.CrackNodeState;

/**
 *
 * @author Philipp Richter
 */
public class Worker extends Observable implements Observer{
    protected final PasswordEntity _passwordEntity;
    protected CrackMethod _crackMethod;
    protected final Client _client;
    protected final ClientRequestTypeFactory _crtf;
    protected final CrackNodeResponseTypeFactory _cnrtf;
    protected volatile Thread _workerThread=null;
    protected volatile ConcurrentHashMap<CrackNode,CrackMethod> _cnmap;
    
    protected volatile CrackNodeResponseType _cnrt;
    protected volatile boolean _found;
    protected volatile boolean _abort;
    protected volatile Socket _socket;
    protected volatile CrackMethod _nullMethod=new NullMethod();

    public Worker(Client client,ClientRequestTypeFactory crtf,CrackNodeResponseTypeFactory cnrtf,PasswordEntity pe,CrackMethod cm){
        _passwordEntity=pe;
        _crackMethod=cm;
        _client=client;
        _crtf=crtf;
        _cnrtf=cnrtf;
        _cnmap=new ConcurrentHashMap<>();
    }
    
    public boolean isAlive(){
        return (_workerThread!=null)?_workerThread.isAlive():false;
    }
    
    public synchronized void stop() throws InterruptedException, IOException{
        if(isAlive()){
            _abort=true;
            if(_socket!=null && !_socket.isClosed()) _socket.close();
            stopAllNodes();
            _workerThread.join(1000);
        }
    }
    
    private synchronized void messagePrinter(String string){
        setChanged();
        notifyObservers(string);
    }
    
    private void messagePrinter(CrackNode cn,CrackNodeResponseType cnrt,String add){
        messagePrinter(cn.getPrettyString()+" "+StringUtils.newStringUtf8(cnrt.getCrackNodeReponseTypeName())+" "+add);
    }
    
    private void messagePrinter(CrackNode cn,CrackNodeResponseType cnrt){
        messagePrinter(cn, cnrt, "");
    }
    
    private void messagePrinter(CrackNode cn,ClientRequestType crt,String add){
        messagePrinter(cn.getPrettyString()+" "+StringUtils.newStringUtf8(crt.getClientRequestTypeName())+" "+add);
    }
    
    private void messagePrinter(CrackNode cn,ClientRequestType crt){
        messagePrinter(cn, crt, "");
    }
    
    private void requestGun(CrackNode cn){
        ClientRequestType prep = _crtf.getClientRequestTypePrepare();
        try {
            _socket = _client.crackNodeConnector(cn);
            messagePrinter(cn, prep);
            _client.sendClientRequest(_socket, prep);
            CrackNodeResponseType crt = _cnrtf.getCrackNodeResponseType(_socket);
            if(crt!=null) messagePrinter(cn, crt);
            if (!prep.getCrackNodeResponseTypeSuccess(_cnrtf).equals(crt)) {
                _socket.close();
                return;
            }
            _socket.close();
            _socket = _client.crackNodeConnector(cn);
            final ClientRequestType start = _crtf.getClientRequestTypeStart(_passwordEntity, _crackMethod);
            messagePrinter(cn, start);
            
            final Thread t=(new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        _client.sendClientRequest(_socket, start);
                        _socket.shutdownOutput();
                    } catch (IOException ex) {
                        Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }));
            t.start();
            try{
                crt = _cnrtf.getCrackNodeResponseType(_socket);
            }catch(IOException e){}
            _socket.shutdownInput();
           if(crt!=null) messagePrinter(cn, crt);
            if (start.getCrackNodeResponseTypeSuccess(_cnrtf).equals(crt)) {
                _cnmap.put(cn, _crackMethod);
                _crackMethod = _crackMethod.getNextChunk();
            }

        } catch (IOException ex) {
            //Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean allDone(){
        for(CrackMethod cm:_cnmap.values()){
            if(!(cm instanceof NullMethod)) return false;
        }
        return true;
    }
    
    private void nodeLoop(){
        do{
            Set<Entry<CrackNode,CrackMethod>> tmp=_cnmap.entrySet();
            for(Map.Entry<CrackNode,CrackMethod> me:tmp){
                if(_crackMethod!=null && (me.getKey().getCrackNodeState()==CrackNodeState.WAITING
                        || me.getKey().getCrackNodeState()==CrackNodeState.UNKNOWN)
                        && (me.getValue() instanceof NullMethod)){
                    requestGun(me.getKey());
                }
            }
        }while(!_abort && !_found && (_crackMethod!=null));
        if(!_found){
            messagePrinter("Password could not be found");
            synchronized(this){
                setChanged();
                notifyObservers(null);
            }
        }
    }
    
    private synchronized void stopAllNodes(){
        messagePrinter("Stopping All Nodes");
        Set<Entry<CrackNode,CrackMethod>> tmp=_cnmap.entrySet();
        for(Map.Entry<CrackNode,CrackMethod> me:tmp){
            if(!(me.getValue() instanceof NullMethod)){
                System.out.println("Stopping "+me.getKey().getPrettyString());
                ClientRequestType stop=_crtf.getClientRequestTypeStop();
                try {
                    Socket socket=_client.crackNodeConnector(me.getKey());
                    _client.sendClientRequest(socket, stop);
                    CrackNodeResponseType crt=_cnrtf.getCrackNodeResponseType(socket);
                    if (stop.getCrackNodeResponseTypeSuccess(_cnrtf).equals(crt)) {
                        _cnmap.replace(me.getKey(), _nullMethod);
                    }
                    socket.close();
                } catch (IOException ex) {
                    //Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private void fillNodeMap(Collection<CrackNode> cncoll){
        _cnmap.clear();
        for(CrackNode cn:cncoll){
            _cnmap.put(cn, _nullMethod);
        }
    }
    
    public synchronized void startJob(Collection<CrackNode> cn) throws IOException{
        messagePrinter("Starting Job with hash '"+_passwordEntity.getCryptCompatibleHashString()+"'");
        _cnrtf.getCrackNodeResponseTypeDone().setPassword(null);
        fillNodeMap(cn);
        _found=false;
        _abort=false;
        _workerThread=new Thread(new Runnable() {

            @Override
            public void run() {
                nodeLoop();
            }
        });
        _workerThread.start();
    }
    
    @Override
    public void update(Observable o, Object arg) {
        if(isAlive()){
            if(o instanceof CrackNodeResponseTypeDone){
                messagePrinter((CrackNode)arg, (CrackNodeResponseTypeDone)o);
                if(arg instanceof CrackNode){
                    if(!(_cnmap.get((CrackNode)arg) instanceof NullMethod)){
                        setChanged();
                        notifyObservers(_cnmap.get((CrackNode)arg).getPercentage());
                    }
                    _cnmap.replace((CrackNode)arg, _nullMethod);
                }
                if(((CrackNodeResponseTypeDone)o).getPassword()!=null){
                    _found=true;
                    (new Thread(new Runnable() {
                        @Override
                        public void run() {
                            stopAllNodes();
                        }
                    })).start();
                    
                    messagePrinter("Password: '"+((CrackNodeResponseTypeDone)o).getPassword()+"'");
                    PasswordEntity pe=new PasswordEntity();
                    pe.setPasswordHashType(_passwordEntity.getPasswordHashType());
                    pe.setSalt(_passwordEntity.getSalt());
                    pe.setPassword(((CrackNodeResponseTypeDone)o).getPassword());
                    synchronized(this){
                        setChanged();
                        notifyObservers(pe);
                    }
                }
                
            }
            else if(arg instanceof CrackNode && _cnmap!=null){
                    _cnmap.putIfAbsent((CrackNode)arg, _nullMethod);
            }
        }
    }
    
}
