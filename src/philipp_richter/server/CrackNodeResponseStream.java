package philipp_richter.server;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.InetAddress;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseStream extends DataInputStream{

    protected final InetAddress _ip;
    
    public CrackNodeResponseStream(InetAddress ip,InputStream in) {
        super(in);
        _ip=ip;
    }
    
    public InetAddress getInetAddress(){
        return _ip;
    }
}
