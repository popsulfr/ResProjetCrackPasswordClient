package philipp_richter.server;

import java.net.Socket;
import java.util.Arrays;
import java.util.Observable;

/**
 *
 * @author Philipp Richter
 */
public abstract class CrackNodeResponseType extends Observable{
    protected final byte[] _crackNodeResponseTypeName;
    
    public CrackNodeResponseType(byte[] crackNodeResponseTypeName){
        _crackNodeResponseTypeName=crackNodeResponseTypeName;
    }
    
    public byte[] getCrackNodeReponseTypeName(){
        return _crackNodeResponseTypeName;
    }
    
    public CrackNodeResponseType handleResponse(Socket sock){
        setChanged();
        notifyObservers(new CrackNode(sock.getInetAddress()));
        return this;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getCrackNodeReponseTypeName());
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof CrackNodeResponseType) && Arrays.equals(getCrackNodeReponseTypeName(), ((CrackNodeResponseType)obj).getCrackNodeReponseTypeName());
    }
}
