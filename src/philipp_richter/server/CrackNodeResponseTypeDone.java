package philipp_richter.server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;
import static philipp_richter.server.Server.next;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeDone extends CrackNodeResponseType{
        
    public static final byte[] DONE_NAME=StringUtils.getBytesUtf8("DONE");
    
    protected String _password=null;
    
    public CrackNodeResponseTypeDone() {
        super(DONE_NAME);
    }
    
    public String getPassword(){
        return _password;
    }
    
    public void setPassword(String s){
        _password=s;
    }

    @Override
    public CrackNodeResponseType handleResponse(Socket sock) {
        byte[] word;
        try {
            if((word=next(new DataInputStream(sock.getInputStream())))!=null){
                _password=StringUtils.newStringUtf8(word);
            }
        } catch (IOException ex) {
            Logger.getLogger(CrackNodeResponseTypeDone.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return super.handleResponse(sock);
    }
}
