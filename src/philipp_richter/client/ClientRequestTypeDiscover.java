package philipp_richter.client;

import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeFactory;
import philipp_richter.server.CrackNodeResponseTypeSignal;

/**
 *
 * @author Philipp Richter
 */
public class ClientRequestTypeDiscover extends ClientRequestType{
    public static final byte[] CLIENT_REQUEST_DISCOVER=StringUtils.getBytesUtf8("DISCOVER");

    public ClientRequestTypeDiscover() {
        super(CLIENT_REQUEST_DISCOVER);
    }

    @Override
    public boolean writeClientRequest(OutputStream os) {
        return true;
    }

    @Override
    public Set<CrackNodeResponseType> getCrackNodeResponseTypeFailure(CrackNodeResponseTypeFactory cnrtf) {
        return new HashSet<>();
    }

    @Override
    public CrackNodeResponseType getCrackNodeResponseTypeSuccess(CrackNodeResponseTypeFactory cnrtf) {
        return cnrtf.getCrackNodeResponseTypeSignal();
    }
}
