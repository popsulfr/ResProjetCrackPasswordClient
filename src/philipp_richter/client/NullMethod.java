package philipp_richter.client;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Philipp Richter
 */
public class NullMethod extends CrackMethod{

    public NullMethod() {
        super("NullMethod");
    }

    @Override
    public void writeClientRequest(OutputStream os) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CrackMethod getNextChunk() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public float getPercentage() {
        return 100f;
    }
}
