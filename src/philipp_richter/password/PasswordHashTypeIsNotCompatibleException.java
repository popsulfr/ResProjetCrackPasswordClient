package philipp_richter.password;

/**
 *
 * @author Philipp Richter
 */
public class PasswordHashTypeIsNotCompatibleException extends PasswordException{
    protected final PasswordHashType _pht;

    public PasswordHashTypeIsNotCompatibleException(PasswordEntity pe, PasswordHashType pht) {
        super(pe,"Password hash '"+pe.getPasswordHash()+"' does not look like a "+pht.toString()+" hash!");
        _pht = pht;
    }
    
    public PasswordHashType getPasswordHashType(){
        return _pht;
    }
}
