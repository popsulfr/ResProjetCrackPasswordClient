package philipp_richter.password;

/**
 *
 * @author Philipp Richter
 */
public class PasswordDoesNotMatchHashException extends PasswordException {
    protected final String _pass;

    /**
     * Constructs an instance of <code>PasswordDoesNotMatchHash</code> with the
     * specified detail message.
     *
     * @param pe
     * @param pass
     */
    public PasswordDoesNotMatchHashException(PasswordEntity pe,String pass) {
        super(pe,"'"+pass+"' does not match hash '"+pe.getPasswordHash()+"'!");
        _pass=pass;
    }
    
    public String getPassword(){
        return _pass;
    }
}
