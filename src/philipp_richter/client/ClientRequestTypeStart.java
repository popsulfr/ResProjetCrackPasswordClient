/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.password.PasswordEntity;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeFactory;

/**
 *
 * @author Philipp Richter
 */
public class ClientRequestTypeStart extends ClientRequestType{
    public static final byte[] CLIENT_REQUEST_START=StringUtils.getBytesUtf8("START");
    protected final PasswordEntity _passwordEntity;
    protected final CrackMethod _crackMethod;

    public ClientRequestTypeStart(PasswordEntity pe,CrackMethod cm) {
        super(CLIENT_REQUEST_START);
        _passwordEntity=pe;
        _crackMethod=cm;
    }

    @Override
    public boolean writeClientRequest(OutputStream os) throws IOException {
        os.write(StringUtils.getBytesUtf8(_passwordEntity.getCryptCompatibleHashString()));
        os.write(Client.CLIENT_SEPARATOR);
        os.write(StringUtils.getBytesUtf8(_crackMethod.getCrackMethodName()));
        os.write(Client.CLIENT_SEPARATOR);
        _crackMethod.writeClientRequest(os);
        return true;
    }

    @Override
    public Set<CrackNodeResponseType> getCrackNodeResponseTypeFailure(CrackNodeResponseTypeFactory cnrtf) {
        Set<CrackNodeResponseType> cnrt=new HashSet<>();
        cnrt.add(cnrtf.getCrackNodeResponseTypeBusy());
        cnrt.add(cnrtf.getCrackNodeResponseTypeNotreserved());
        cnrt.add(cnrtf.getCrackNodeResponseTypeBadclient());
        return cnrt;
    }

    @Override
    public CrackNodeResponseType getCrackNodeResponseTypeSuccess(CrackNodeResponseTypeFactory cnrtf) {
        return cnrtf.getCrackNodeResponseTypeAccepted();
    }
       
}
