/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Philipp Richter
 */
public class Alphabet {
    protected String _name;
    protected String _alphabet;
    
    public Alphabet(String name,String alphabet){
        _name=name;
        setAlphabet(alphabet);
    }
    
    public String getName(){
        return _name;
    }
    
    public String getAlphabet(){
        return _alphabet;
    }
    
    public void setName(String name){
        _name=name;
    }
    
    public int getSize(){
        return _alphabet.length();
    }
    
    public void setAlphabet(String alphabet){
        Set<Character> tmp=new HashSet<>();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<alphabet.length();++i){
            if(!tmp.contains(alphabet.charAt(i))){
                tmp.add(alphabet.charAt(i));
                sb.append(alphabet.charAt(i));
            }
        }
        _alphabet=sb.toString();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof Alphabet) && (getName().equals(((Alphabet)o).getName())));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"{"+getName()+"$"+getAlphabet()+"}";
    }
}
