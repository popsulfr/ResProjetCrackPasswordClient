package philipp_richter.client;

import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.codec.binary.StringUtils;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeBusy;
import philipp_richter.server.CrackNodeResponseTypeFactory;
import philipp_richter.server.CrackNodeResponseTypeReady;

/**
 *
 * @author Philipp Richter
 */
public class ClientRequestTypePrepare extends ClientRequestType{
    public static final byte[] CLIENT_REQUEST_PREPARE=StringUtils.getBytesUtf8("PREPARE");
    
    public ClientRequestTypePrepare(){
        super(CLIENT_REQUEST_PREPARE);
    }

    @Override
    public boolean writeClientRequest(OutputStream os) {
        return true;
    }

    @Override
    public Set<CrackNodeResponseType> getCrackNodeResponseTypeFailure(CrackNodeResponseTypeFactory cnrtf) {
        Set<CrackNodeResponseType> cnrt=new HashSet<>();
        cnrt.add(cnrtf.getCrackNodeResponseTypeBusy());
        return cnrt;
    }

    @Override
    public CrackNodeResponseType getCrackNodeResponseTypeSuccess(CrackNodeResponseTypeFactory cnrtf) {
        return cnrtf.getCrackNodeResponseTypeReady();
    }
}
