/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Philipp Richter
 */
public abstract class CrackMethod {
    
    public static final int CHUNK_SIZE=8192;
    protected String _name;
    
    public CrackMethod(String name){
        _name=name;
    }
    
    public String getCrackMethodName(){
        return _name;
    }

    @Override
    public int hashCode() {
        return getCrackMethodName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof CrackMethod) && getCrackMethodName().equals(((CrackMethod)obj).getCrackMethodName());
    }
    
    public abstract void writeClientRequest(OutputStream os) throws IOException;
    
    public abstract CrackMethod getNextChunk();
    
    public abstract float getPercentage();
}
