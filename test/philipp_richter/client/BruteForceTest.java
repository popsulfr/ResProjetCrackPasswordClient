/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.client;

import java.io.OutputStream;
import java.math.BigInteger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Philipp Richter
 */
public class BruteForceTest {

    /**
     * Test of getMaxCombinations method, of class BruteForce.
     */
    @Test
    public void testGetMaxCombinations() {
        BruteForce bf=new BruteForce(new Alphabet("test", "abcdefghijklmnopqrstuvwxyz0123456789"), 5, 16);
        BigInteger maxcomb=bf.getMaxCombinations();
        System.out.println("Maxcomb: "+maxcomb.toString(10));
    }

    /**
     * Test of getWordAtPos method, of class BruteForce.
     */
    @Test
    public void testGetWordAtPos() {
        BruteForce bf=new BruteForce(new Alphabet("test", "0123456789abcdefghijklmnopqrstuvwxyz"), 1, 16);
        BigInteger maxcomb=bf.getMaxCombinations();
        System.out.println("Maxcomb: "+maxcomb.toString(10));
        BigInteger pos=maxcomb.subtract(BigInteger.ONE);
        String word=bf.getWordAtPos(BigInteger.ZERO);
        System.out.println("Word: "+word);
    }
    
}
