package philipp_richter.server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeSignal extends CrackNodeResponseType{

    public static final byte[] SIGNAL_NAME=StringUtils.getBytesUtf8("SIGNAL");
    
    public CrackNodeResponseTypeSignal() {
        super(SIGNAL_NAME);
    }

    @Override
    public CrackNodeResponseType handleResponse(Socket cnrs) {
        CrackNode cn=new CrackNode(cnrs.getInetAddress());
        byte[] word;
        try {
            if((word=Server.next(new DataInputStream(cnrs.getInputStream())))!=null){
                for(CrackNodeState cns:CrackNodeState.values()){
                    if(Arrays.equals(StringUtils.getBytesUtf8(cns.toString()), word)){
                        cn.setCrackNodeState(cns);
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CrackNodeResponseTypeSignal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        setChanged();
        notifyObservers(cn);
        return this;
    }
}
