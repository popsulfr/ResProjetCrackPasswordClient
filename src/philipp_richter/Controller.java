package philipp_richter;

import philipp_richter.password.PasswordHashType;
import philipp_richter.password.PasswordEntity;
import philipp_richter.gui.MainGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.RowFilter;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableRowSorter;
import philipp_richter.client.Alphabet;
import philipp_richter.client.BruteForce;
import philipp_richter.client.Client;
import philipp_richter.client.ClientRequestTypeFactory;
import philipp_richter.client.CrackMethod;
import philipp_richter.client.DictMethod;
import philipp_richter.client.RainbowMethod;
import philipp_richter.server.CrackNodeResponseTypeFactory;
import philipp_richter.server.CrackNodeState;
import philipp_richter.server.CrackNodeTableModel;
import philipp_richter.server.Server;

/**
 *
 * @author Philipp Richter
 */
public class Controller implements Observer{
    public static final int DEFAULT_TIMER_PERIOD=10;
    protected MainGUI _mainGUI;
    protected PasswordEntity _passwordEntity;
    protected Server _server;
    protected CrackNodeResponseTypeFactory _cnrtf;
    protected ClientRequestTypeFactory _crtf;
    protected Client _client;
    protected Timer _timer;
    protected volatile int _timerPeriod;
    protected CrackNodeTableModel _cntm;
    protected volatile Worker _worker=null;
    
    protected File _dictFile=null;
    protected File _rainbowFile=null;
    
    protected TableRowSorter<CrackNodeTableModel> _sorter;
    protected RowFilter<CrackNodeTableModel,Object> _filterBusy;
    protected RowFilter<CrackNodeTableModel,Object> _filterFree;
    
    public Controller(MainGUI mainGUI,Server serv,Client client){
        _mainGUI=mainGUI;
        _passwordEntity=null;
        _server=serv;
        _cnrtf=new CrackNodeResponseTypeFactory();
        _crtf=new ClientRequestTypeFactory();
        _client=client;
        _cntm=new CrackNodeTableModel();
        initComponents();
        initModels();
        initObservers();
        initActionListeners();
        initConsoleTextPane();
        initBroadcastTimer();
    }
    
    private void initComponents(){
        _timerPeriod=DEFAULT_TIMER_PERIOD;
        _mainGUI.setRefreshButtonText("Refresh "+_timerPeriod+"s");
    }
    
    private void filterChecker(){
        if(_mainGUI.getBusyToggleButtonState() && _mainGUI.getFreeToggleButtonState()){
            _sorter.setRowFilter(null);
        }
        else if(!_mainGUI.getBusyToggleButtonState() && !_mainGUI.getFreeToggleButtonState()){
            _sorter.setRowFilter(RowFilter.regexFilter("^"+CrackNodeState.UNKNOWN+"$", 1));
        }
        else if(_mainGUI.getBusyToggleButtonState()){
            _sorter.setRowFilter(_filterBusy);
        }
        else if(_mainGUI.getFreeToggleButtonState()){
            _sorter.setRowFilter(_filterFree);
        }
    }
    private void initModels(){
        _sorter=new TableRowSorter<>(_cntm);
        RowFilter.regexFilter("^"+CrackNodeState.BUSY+"$");
        _filterBusy=RowFilter.regexFilter("^"+CrackNodeState.BUSY+"$",1);
        _filterFree=RowFilter.regexFilter("^"+CrackNodeState.WAITING+"$",1);
        _mainGUI.setCrackNodeTableModel(_cntm);
        filterChecker();
        _mainGUI.setCrackNodeTableSorter(_sorter);
    }
    
    private void initObservers(){
        _server.addObserver(_cnrtf);
        _cnrtf.getCrackNodeResponseTypeSignal().addObserver(_cntm);
    }
    
    private void initConsoleTextPane(){
        _mainGUI.clearConsoleTextPane();
        _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"Hello");
    }
            
    private void initActionListeners(){
        _mainGUI.addActionListenerMagicButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                _passwordEntity=new PasswordEntity();
                fillPasswordInfo();
            }
        });
        _mainGUI.addActionListenerAutofillButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                autofillPasswordInfo();
            }
        });
        _mainGUI.addActionListenerRefreshButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                _timer.stop();
                _timerPeriod=1;
                _timer.setInitialDelay(0);
                _timer.start();
            }
        });
        
        _mainGUI.addActionListenerCrackButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(_cntm.getRowCount()>0){
                    _mainGUI.setPasswordToolBoxState(false);
                    if(!workerCreator()){
                        _mainGUI.setPasswordToolBoxState(true);
                        _mainGUI.setAbortButtonState(false);
                    }
                    else _mainGUI.setAbortButtonState(true);
                }
                else{
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: No nodes");
                }
            }
        });
        
        _mainGUI.addActionListenerAbortButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(_worker!=null){
                    try {
                        _worker.stop();
                    } catch (InterruptedException ex) {
                        //Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        //Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                _mainGUI.setPasswordToolBoxState(true);
                _mainGUI.setAbortButtonState(false);
            }
        });
        
        _mainGUI.addActionListenerDictOpenButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if((_dictFile=_mainGUI.getDictFile())!=null){
                    _mainGUI.setDictFileTextfield(_dictFile.getAbsolutePath());
                    _mainGUI.setDictFileInfoLabel("Size: "+(_dictFile.length()/1024)+"KiB");
                }
            }
        });
        
        _mainGUI.addActionListenerRainbowOpenButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if((_rainbowFile=_mainGUI.getRainbowFile())!=null){
                    _mainGUI.setRainbowFileTextfield(_rainbowFile.getAbsolutePath());
                    _mainGUI.setRainbowFileInfoLabel("Size: "+(_rainbowFile.length()/1024)+"KiB");
                }
            }
        });
        
        _mainGUI.addChangeListenerBusyToggleButton(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                filterChecker();
            }
        });
        
        _mainGUI.addChangeListenerFreeToggleButton(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                filterChecker();
            }
        });
        
        _mainGUI.addActionListenerCryptHashButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(_mainGUI.getCryptHashButtonText().equals("H")){
                    _mainGUI.setCryptHashButtonText("C");
                    fillPasswordInfo();
                }
                else{
                    _mainGUI.setCryptHashButtonText("H");
                    fillPasswordInfo();
                }
            }
        });
    }
    
    private boolean workerCreator(){
        if(!autofillPasswordInfo() || _passwordEntity.getPasswordHash()==null || _passwordEntity.getPasswordHash().isEmpty()){
            _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Errors Exist with Password!");
            return false;
        }
        CrackMethod cm=null;
        switch(_mainGUI.getActiveCrackMethodPane()){
            case 0:
                if(_mainGUI.getAlphabetTextArea().isEmpty()){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Alphabet is empty");
                    return false; 
                }
                if(_mainGUI.getMinWordSpinner()>_mainGUI.getMaxWordSpinner()){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Minimum Word Size is bigger than maximum word size");
                    return false;
                }
                cm=new BruteForce(new Alphabet("truc",_mainGUI.getAlphabetTextArea()),_mainGUI.getMinWordSpinner(),_mainGUI.getMaxWordSpinner());
                break;
            case 1:
                if(_dictFile==null){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Dictionary was not supplied");
                    return false;
                }
                if(!_dictFile.canRead()){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Dictionary is not readable");
                    return false;
                }
                cm=new DictMethod(_dictFile);
                break;
            case 2:
                if(_rainbowFile==null){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Rainbow table was not supplied");
                    return false;
                }
                if(!_rainbowFile.canRead()){
                    _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+"ERROR: Rainbow table is not readable");
                    return false;
                }
                cm=new RainbowMethod(_rainbowFile);
            default: ;
        }
        _mainGUI.setCrackProgressbarState(0);
        _worker=new Worker(_client, _crtf, _cnrtf, _passwordEntity,cm);
        _cnrtf.addObserverToAll(_worker);
        _worker.addObserver(this);
        try {
            _worker.startJob(_cntm.getCrackNodes());
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
    private void initBroadcastTimer(){
        try {
            _client.broadCast();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        _timer=new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                _mainGUI.setRefreshButtonText("Refresh "+--_timerPeriod+"s");
                if(_timerPeriod<=0){
                    _mainGUI.setRefreshButtonState(false);
                    _timerPeriod=DEFAULT_TIMER_PERIOD;
                    _cntm.clearTable();
                    try {
                        _client.broadCast();
                    } catch (IOException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    _mainGUI.setRefreshButtonState(true);
                }
            }
        });
        _timer.setInitialDelay(1000);
        _timer.start();
    }
    
    private void fillPasswordInfo(){
        if(_passwordEntity!=null){
            _mainGUI.setPasswordHashTextField((_mainGUI.getCryptHashButtonText().equals("C"))?
                    ((_passwordEntity.getCryptCompatibleHashString()!=null)?_passwordEntity.getCryptCompatibleHashString():""):
                    (_passwordEntity.getPasswordHash()!=null)?_passwordEntity.getPasswordHash():"");
            _mainGUI.setSaltTextField((_passwordEntity.getSalt()!=null)?_passwordEntity.getSalt():"");
            _mainGUI.setPasswordHashType((_passwordEntity.getPasswordHashType()!=null)?_passwordEntity.getPasswordHashType():PasswordHashType.MD5);
            _mainGUI.setPasswordTextField((_passwordEntity.getPassword()!=null)?_passwordEntity.getPassword():"");
        }
    }
    
    private boolean autofillPasswordInfo(){
        try{
        if(_passwordEntity==null){
            _passwordEntity=new PasswordEntity();
        }
        PasswordEntity pe=new PasswordEntity();
        if(!_mainGUI.getPasswordHashTextField().isEmpty() && !_mainGUI.getPasswordHashTextField().equals(
                (_passwordEntity.getPasswordHash()!=null)?_passwordEntity.getPasswordHash().trim():"")){
            if(_mainGUI.getCryptHashButtonText().equals("C")){
                pe=PasswordEntity.createPasswordEntityFromCryptHashString(_mainGUI.getPasswordHashTextField().trim());
                if(!_mainGUI.getPasswordTextField().isEmpty()){
                    pe.setPassword(_mainGUI.getPasswordTextField());
                }
            }
            else{
                pe.setPasswordHash(_mainGUI.getPasswordHashTextField().trim());
                if(!_mainGUI.getSaltTextField().isEmpty()){
                    pe.setSalt(_mainGUI.getSaltTextField().trim());
                }
                if(_mainGUI.getPasswordHashType()!=pe.getPasswordHashType()){
                    pe.setPasswordHashType(_mainGUI.getPasswordHashType());
                }
                if(!_mainGUI.getPasswordTextField().isEmpty()){
                    pe.setPassword(_mainGUI.getPasswordTextField());
                }
            }
        }
        else{
            /*
            if(!_mainGUI.getPasswordHashTextField().trim().isEmpty()
                    && (_passwordEntity.getPasswordHash()==null
                    || !_mainGUI.getPasswordHashTextField().trim().equals(_passwordEntity.getPasswordHash()))){
                pe.setPasswordHash(_mainGUI.getPasswordHashTextField().trim());
            }
            else if(_passwordEntity.getPasswordHash()!=null && !_passwordEntity.getPasswordHash().isEmpty()) pe.setPasswordHash(_passwordEntity.getPasswordHash());
                    */
            if(_mainGUI.getPasswordHashType()!=_passwordEntity.getPasswordHashType()){
                pe.setPasswordHashType(_mainGUI.getPasswordHashType());
            }
            if(!_mainGUI.getSaltTextField().trim().isEmpty()
                    && (_passwordEntity.getSalt()==null || !_mainGUI.getSaltTextField().equals(_passwordEntity.getSalt()))){
                pe.setSalt(_mainGUI.getSaltTextField().trim());
            }
            else if(_passwordEntity.getSalt()!= null && !_passwordEntity.getSalt().isEmpty()) pe.setSalt(_passwordEntity.getSalt());
            if(!_mainGUI.getPasswordTextField().isEmpty() &&
                    (_passwordEntity.getPassword()==null
                    || !_mainGUI.getPasswordTextField().equals(_passwordEntity.getPassword()))){
                pe.setPassword(_mainGUI.getPasswordTextField());
            }
            else if(_passwordEntity.getPassword()!=null && !_passwordEntity.getPassword().isEmpty()) pe.setPassword(_passwordEntity.getPassword());
        }
        _passwordEntity=pe;
        fillPasswordInfo();
        return true;
        }catch(Exception e){
            //System.err.println(e.getClass().getSimpleName());
            //Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
            _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+e.getMessage());
            return false;
        }
    }
    
    private String getCurrentTimestamp(){
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar cal=Calendar.getInstance();
        return "["+df.format(cal.getTime())+"] ";
    }

    @Override
    public void update(Observable o, Object arg){
        if(o instanceof Worker){
            if(arg instanceof PasswordEntity){
                _mainGUI.setCrackProgressbarState(100);
                _passwordEntity=(PasswordEntity)arg;
                fillPasswordInfo();
                _mainGUI.setPasswordToolBoxState(true);
                _mainGUI.setAbortButtonState(false);
            }
            else if(arg instanceof String){
                _mainGUI.appendStringToConsoleTextPane(getCurrentTimestamp()+(String)arg);
            }
            else if(arg instanceof Float){
                _mainGUI.setCrackProgressbarState(Math.round((Float)arg));
            }
            else{
                _mainGUI.setPasswordToolBoxState(true);
                _mainGUI.setAbortButtonState(false);
            }
        }
    }
}
