package philipp_richter.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class DictMethod extends CrackMethod{

    public static final String DICT_NAME="DICT";
    
    protected final File _file;
    
    protected long _currentOffset=0;
    
    public DictMethod(File file) {
        super(DICT_NAME);
        _file=file;
    }
    
    public File getFile(){
        return _file;
    }

    @Override
    public void writeClientRequest(OutputStream os) throws IOException {
        RandomAccessFile raf=new RandomAccessFile(getFile(), "r");
        raf.seek(_currentOffset);
        String line;
        for(int i=0;i<CrackMethod.CHUNK_SIZE && (line=raf.readLine())!=null;++i){
            os.write(StringUtils.getBytesUtf8(line));
            os.write('\n');
        }
    }

    @Override
    @SuppressWarnings("empty-statement")
    public CrackMethod getNextChunk() {
        if(_currentOffset<getFile().length()){
            try {
                RandomAccessFile raf=new RandomAccessFile(getFile(), "r");
                raf.seek(_currentOffset);
                for(int i=0;i<CrackMethod.CHUNK_SIZE && (raf.readLine()!=null);++i){};
                if(raf.getFilePointer()<getFile().length()){
                    DictMethod dm=new DictMethod(getFile());
                    dm._currentOffset=raf.getFilePointer();
                    raf.close();
                    return dm;
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DictMethod.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DictMethod.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public float getPercentage() {
        return ((float)(_currentOffset)/getFile().length())*100f;
    }
    
    
}
