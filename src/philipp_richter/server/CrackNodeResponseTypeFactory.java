package philipp_richter.server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;
import static philipp_richter.server.Server.SERVER_NODE_HEADER;
import static philipp_richter.server.Server.next;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeFactory implements Observer{
    protected final CrackNodeResponseTypeSignal _crackNodeResponseTypeSignal;
    protected final CrackNodeResponseTypeReady _crackNodeResponseTypeReady;
    protected final CrackNodeResponseTypeBusy _crackNodeResponseTypeBusy;
    protected final CrackNodeResponseTypeAccepted _crackNodeResponseTypeAccepted;
    protected final CrackNodeResponseTypeAborted _crackNodeResponseTypeAborted;
    protected final CrackNodeResponseTypeDone _crackNodeResponseTypeDone;
    protected final CrackNodeResponseTypeNotrunning _crackNodeResponseTypeNotrunning;
    protected final CrackNodeResponseTypeBadclient _crackNodeResponseTypeBadclient;
    protected final CrackNodeResponseTypeNotreserved _crackNodeResponseTypeNotreserved;
    
    public CrackNodeResponseTypeFactory(){
        _crackNodeResponseTypeSignal=new CrackNodeResponseTypeSignal();
        _crackNodeResponseTypeReady=new CrackNodeResponseTypeReady();
        _crackNodeResponseTypeBusy=new CrackNodeResponseTypeBusy();
        _crackNodeResponseTypeAccepted=new CrackNodeResponseTypeAccepted();
        _crackNodeResponseTypeAborted=new CrackNodeResponseTypeAborted();
        _crackNodeResponseTypeDone=new CrackNodeResponseTypeDone();
        _crackNodeResponseTypeNotrunning=new CrackNodeResponseTypeNotrunning();
        _crackNodeResponseTypeBadclient=new CrackNodeResponseTypeBadclient();
        _crackNodeResponseTypeNotreserved=new CrackNodeResponseTypeNotreserved();
    }
    public CrackNodeResponseTypeSignal getCrackNodeResponseTypeSignal(){
        return _crackNodeResponseTypeSignal;
    }
    
    public CrackNodeResponseTypeReady getCrackNodeResponseTypeReady(){
        return _crackNodeResponseTypeReady;
    }
    
    public CrackNodeResponseTypeBusy getCrackNodeResponseTypeBusy(){
        return _crackNodeResponseTypeBusy;
    }
    
    public CrackNodeResponseTypeAccepted getCrackNodeResponseTypeAccepted(){
        return _crackNodeResponseTypeAccepted;
    }

    public CrackNodeResponseTypeAborted getCrackNodeResponseTypeAborted(){
        return _crackNodeResponseTypeAborted;
    }
    
    public CrackNodeResponseTypeDone getCrackNodeResponseTypeDone(){
        return _crackNodeResponseTypeDone;
    }
    
    public CrackNodeResponseTypeNotrunning getCrackNodeResponseTypeNotrunning(){
        return _crackNodeResponseTypeNotrunning;
    }
    
    public CrackNodeResponseTypeBadclient getCrackNodeResponseTypeBadclient(){
        return _crackNodeResponseTypeBadclient;
    }
    
    public CrackNodeResponseTypeNotreserved getCrackNodeResponseTypeNotreserved(){
        return _crackNodeResponseTypeNotreserved;
    }
    
    private CrackNodeResponseType handleServerResponse(Socket sock) throws IOException{
        byte[] word;
        DataInputStream dis=new DataInputStream(sock.getInputStream());
        if((word=Server.next(dis))!=null){
            if(Arrays.equals(getCrackNodeResponseTypeSignal().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeSignal().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeReady().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeReady().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeBusy().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeBusy().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeAccepted().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeAccepted().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeAborted().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeAborted().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeDone().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeDone().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeNotrunning().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeNotrunning().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeBadclient().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeBadclient().handleResponse(sock);
            }
            else if(Arrays.equals(getCrackNodeResponseTypeNotreserved().getCrackNodeReponseTypeName(), word)){
                return getCrackNodeResponseTypeNotreserved().handleResponse(sock);
            }
        }
        return null;
    }
    
    public CrackNodeResponseType getCrackNodeResponseType(Socket sock) throws IOException{
        byte[] word;
        if((word=next(new DataInputStream(sock.getInputStream())))!=null){
            if(Arrays.equals(SERVER_NODE_HEADER, word)){
                return handleServerResponse(sock);
            }
        }
        return null;
    }
    
    public void deleteObserverFromAll(Observer o){
        _crackNodeResponseTypeSignal.deleteObserver(o);
        _crackNodeResponseTypeReady.deleteObserver(o);
        _crackNodeResponseTypeBusy.deleteObserver(o);
        _crackNodeResponseTypeAccepted.deleteObserver(o);
        _crackNodeResponseTypeAborted.deleteObserver(o);
        _crackNodeResponseTypeDone.deleteObserver(o);
        _crackNodeResponseTypeNotrunning.deleteObserver(o);
        _crackNodeResponseTypeBadclient.deleteObserver(o);
        _crackNodeResponseTypeNotreserved.deleteObserver(o);
    }
    
    public void addObserverToAll(Observer o){
        _crackNodeResponseTypeSignal.addObserver(o);
        _crackNodeResponseTypeReady.addObserver(o);
        _crackNodeResponseTypeBusy.addObserver(o);
        _crackNodeResponseTypeAccepted.addObserver(o);
        _crackNodeResponseTypeAborted.addObserver(o);
        _crackNodeResponseTypeDone.addObserver(o);
        _crackNodeResponseTypeNotrunning.addObserver(o);
        _crackNodeResponseTypeBadclient.addObserver(o);
        _crackNodeResponseTypeNotreserved.addObserver(o);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof Server && arg instanceof Socket){
            try {
                handleServerResponse((Socket)arg);
            } catch (IOException ex) {
                Logger.getLogger(CrackNodeResponseTypeFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
