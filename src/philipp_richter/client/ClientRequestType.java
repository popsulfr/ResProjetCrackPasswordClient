package philipp_richter.client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Set;
import philipp_richter.server.CrackNodeResponseType;
import philipp_richter.server.CrackNodeResponseTypeFactory;

/**
 *
 * @author Philipp Richter
 */
public abstract class ClientRequestType {
    protected final byte[] _clientRequestTypeName;
    
    public ClientRequestType(byte[] clientRequestTypeName){
        _clientRequestTypeName=clientRequestTypeName;
    }
    
    public byte[] getClientRequestTypeName(){
        return _clientRequestTypeName;
    }
    
    public abstract boolean writeClientRequest(OutputStream os) throws IOException;
    
    public abstract CrackNodeResponseType getCrackNodeResponseTypeSuccess(CrackNodeResponseTypeFactory cnrtf);
    public abstract Set<CrackNodeResponseType> getCrackNodeResponseTypeFailure(CrackNodeResponseTypeFactory cnrtf);

    @Override
    public int hashCode() {
        return Arrays.hashCode(getClientRequestTypeName());
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof ClientRequestType) && Arrays.equals(getClientRequestTypeName(), ((ClientRequestType)obj).getClientRequestTypeName());
    }
    
}
