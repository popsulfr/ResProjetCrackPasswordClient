package philipp_richter.server;

import java.net.Socket;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeResponseTypeNotreserved extends CrackNodeResponseType{
        
    public static final byte[] NOTRESERVED_NAME=StringUtils.getBytesUtf8("NOTRESERVED");
    
    public CrackNodeResponseTypeNotreserved() {
        super(NOTRESERVED_NAME);
    }
}
