package philipp_richter.server;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Philipp Richter
 */
public class CrackNodeTableModel extends AbstractTableModel implements Observer{
    protected volatile List<CrackNode> _crackNodes;
    
    public CrackNodeTableModel(){
        _crackNodes=new LinkedList<>();
    }

    @Override
    public int getRowCount() {
        return _crackNodes.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        CrackNode cn=_crackNodes.get(rowIndex);
        switch(columnIndex){
            case 0:
                return cn.getIP().getHostAddress();
            case 1:
                return cn.getCrackNodeState();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "IP";
            case 1:
                return "State";
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    public synchronized void addCrackNode(CrackNode cn){
        int pos;
        if((pos=_crackNodes.indexOf(cn))>-1){
            _crackNodes.set(pos, cn);
            fireTableRowsUpdated(pos, pos);
        }
        else{
            _crackNodes.add(cn);
            fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
        }
    }
    
    public synchronized void clearTable(){
        _crackNodes.clear();
        fireTableDataChanged();
    }
    
    public List<CrackNode> getCrackNodes(){
        return _crackNodes;
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof CrackNode){
            addCrackNode((CrackNode)arg);
        }
    }
}
