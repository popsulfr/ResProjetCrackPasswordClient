/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter;

import philipp_richter.password.PasswordEntity;
import philipp_richter.password.PasswordHashTypeIsNotCompatibleException;
import philipp_richter.password.PasswordHashType;
import philipp_richter.password.SaltDoesNotMatchHashPasswordException;
import philipp_richter.password.PasswordDoesNotMatchHashException;
import philipp_richter.password.HashDoesNotMatchPasswordException;
import org.apache.commons.codec.digest.Crypt;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Philipp Richter
 */
public class PasswordEntityTest {
    protected String _password;
    protected String _salt;
    
    public PasswordEntityTest(){
        _password="hello";
        _salt="abcdefgh";
    }
    
    private void encryptAndTest(String password,String salt,PasswordHashType pht){
        String HS=pht.encrypt(password,salt);
        System.out.println(pht.toString()+" Encrypt: '"+HS);
        PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(HS);
        System.out.println(pht.toString()+" PasswordEntity: '"+pe.getCryptCompatibleHashString()+"'");
        assertEquals(HS,pe.getCryptCompatibleHashString());
        assertEquals(salt.substring(0,(salt.length()>pht.getSaltMaxLength())?pht.getSaltMaxLength():salt.length()),pe.getSalt());
        assertEquals(pht,pe.getPasswordHashType());
    }
    
    /**
     * Test of createPasswordEntityFromCryptHashString method, of class PasswordEntity.
     */
    @Test
    public void testCreatePasswordEntityFromCryptHashString() {
        System.out.println("testCreatePasswordEntityFromCryptHashString()");
        System.out.println("Password: '"+_password+"'");
        System.out.println("Salt: '"+_salt+"'");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            encryptAndTest(_password, _salt, pht);
        }
    }

    /**
     * Test of getPasswordHash method, of class PasswordEntity.
     */
    @Test
    public void testGetPasswordHash() {
    }

    /**
     * Test of getSalt method, of class PasswordEntity.
     */
    @Test
    public void testGetSalt() {
    }

    /**
     * Test of getPasswordHashType method, of class PasswordEntity.
     */
    @Test
    public void testGetPasswordHashType() {
    }

    /**
     * Test of getPassword method, of class PasswordEntity.
     */
    @Test
    public void testGetPassword() {
    }

    /**
     * Test of getCryptCompatibleSaltParameter method, of class PasswordEntity.
     */
    @Test
    public void testGetCryptCompatibleSaltParameter() {
    }

    /**
     * Test of getCryptCompatibleHashString method, of class PasswordEntity.
     */
    @Test
    public void testGetCryptCompatibleHashString() {
        
    }

    /**
     * Test of setPasswordHash method, of class PasswordEntity.
     */
    @Test
    public void testSetPasswordHash() {
        System.out.println("testSetPasswordHash()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            String s=pht.encrypt(_password, _salt);
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(s);
            pe.setPassword(_password);
            pe.setPasswordHash(pht.extractHash(s));
            try{
                pe.setPasswordHash(pht.extractHash(s).substring(1)+"a");
                fail("The false hash shouldn't have matched the password");
            }
            catch(HashDoesNotMatchPasswordException e){}
        }
    }
    
    @Test
    public void testPasswordHashMatches(){
        System.out.println("testPasswordHashMatches()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            String hs=pht.encrypt(_password, _salt);
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(hs);
            pe.setPassword(_password);
            assertTrue(pe.passwordHashMatches(pht.extractHash(hs)));
            assertFalse(pe.passwordHashMatches(pht.extractHash(hs).substring(1)+"i"));
        }
    }

    /**
     * Test of setSalt method, of class PasswordEntity.
     */
    @Test
    public void testSetSalt() {
        System.out.println("testSetSalt()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(pht.encrypt(_password, _salt));
            pe.setPassword(_password);
            pe.setSalt(_salt);
            try{
                pe.setSalt(_salt.substring(1)+"i");
                fail("Salt with the password shouldn't have matched the hash");
            }
            catch(SaltDoesNotMatchHashPasswordException e){}
        }
    }
    
    @Test
    public void testSaltMatches(){
        System.out.println("testSaltMatches()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(pht.encrypt(_password, _salt));
            pe.setPassword(_password);
            assertTrue(pe.saltMatches(_salt));
            assertFalse(pe.saltMatches(_salt.substring(1)+"i"));
        }
    }

    /**
     * Test of setPasswordHashType method, of class PasswordEntity.
     */
    @Test
    public void testSetPasswordHashType() {
        System.out.println("testSetPasswordHashType()");
        for(int i=0;i<PasswordHashType.values().length;++i){
            if(PasswordHashType.values()[i]==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(PasswordHashType.values()[i].encrypt(_password, _salt));
            pe.setPassword(_password);
            pe.setPasswordHashType(PasswordHashType.values()[i]);
            try{
                pe.setPasswordHashType(PasswordHashType.values()[(i+1)%PasswordHashType.values().length]);
                fail("Password HashType should not be compatible with the hash");
            }
            catch(PasswordHashTypeIsNotCompatibleException e){}
        }
    }

    /**
     * Test of setPassword method, of class PasswordEntity.
     */
    @Test
    public void testSetPassword() {
        System.out.println("testSetPassword()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(pht.encrypt(_password, _salt));
            pe.setPassword(_password);
            try{
                pe.setPassword(_password+"i");
                fail("Password should have not matched");
            }
            catch(PasswordDoesNotMatchHashException e){}
        }
    }

    /**
     * Test of passwordMatches method, of class PasswordEntity.
     */
    @Test
    public void testPasswordMatches() {
        System.out.println("testPasswordMatches()");
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht==PasswordHashType.UNKNOWN_HASH_TYPE) break;
            PasswordEntity pe=PasswordEntity.createPasswordEntityFromCryptHashString(pht.encrypt(_password, _salt));
            assertTrue(pe.passwordMatches(_password));
            assertFalse(pe.passwordHashMatches(_password+"i"));
        }
    }
    
}
