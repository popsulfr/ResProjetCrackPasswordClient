package philipp_richter.server;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Philipp Richter
 */
public class Server extends Observable{
    public static final int SERVER_PORT=1664;
    public static final byte[] SERVER_NODE_HEADER=StringUtils.getBytesUtf8("CRACKNODE");
    public static final byte SERVER_SEPARATOR='%';
    public static final long SERVER_STOP_TIMEOUT=2000;
    
    protected volatile int _port=0;
    protected volatile Thread _listenThread=null;
    protected volatile ServerSocket _servSock=null;
    
    public Server(int port){
        _port=port;
    }
    
    public Server(){
    }
    
    public int getPort(){
        return _port;
    }
    
    public boolean isListening(){
        return (_listenThread!=null)?_listenThread.isAlive():false;
    }
    
    public synchronized void stopListening() throws IOException, InterruptedException{
        if(isListening()){
            _servSock.close();
            _servSock=null;
            _listenThread.join(SERVER_STOP_TIMEOUT);
            _listenThread=null;
        }
    }
    
    public static byte[] next(InputStream dis) throws IOException{
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        int b;
        for(int i=0;((b=dis.read())>=0) && (b!=SERVER_SEPARATOR);++i){
            baos.write(b);
        }
        return (baos.size()>0)?baos.toByteArray():null;
    }
    
    /**
     * We check the server connection header and delegate the rest of the operations to the ServerConnectionHandler
     * @param sock
     * @throws IOException 
     */
    private void handleConnection(Socket sock) throws IOException{
        byte[] word;
        if((word=next(new DataInputStream(sock.getInputStream())))!=null){
            if(Arrays.equals(SERVER_NODE_HEADER, word)){
                setChanged();
                notifyObservers(sock);
            }
        }
    }
    
    public synchronized void listen() throws IOException{
        if(isListening()){
            throw new ServerIsAlreadyListeningException();
        }
        _servSock=new ServerSocket(getPort());
        _servSock.setReuseAddress(true);
        if(getPort()==0) _port=_servSock.getLocalPort();
        _listenThread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (;;) {
                        final Socket sock = _servSock.accept();
                        (new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    handleConnection(sock);
                                    sock.close();
                                } catch (IOException ex) {
                                    //Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        })).start();
                    }
                } catch (IOException ex) {
                    //Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        _listenThread.start();
    }
}
