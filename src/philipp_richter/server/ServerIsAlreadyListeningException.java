package philipp_richter.server;

/**
 *
 * @author Philipp Richter
 */
public class ServerIsAlreadyListeningException extends RuntimeException {

    /**
     * Creates a new instance of <code>ServerIsAlreadyListeningException</code>
     * without detail message.
     */
    public ServerIsAlreadyListeningException() {
        super("The server is already listening");
    }
}
