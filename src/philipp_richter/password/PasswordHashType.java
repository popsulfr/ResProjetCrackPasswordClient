/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package philipp_richter.password;

import philipp_richter.mydigest.MyMd5Crypt;
import philipp_richter.mydigest.MySha2Crypt;
import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.codec.digest.UnixCrypt;

/**
 *
 * @author Philipp Richter
 */
public enum PasswordHashType {
    DES(0,11,2),MD5(1,22,8),SHA256(5,43,16),SHA512(6,86,16),UNKNOWN_HASH_TYPE(8,0,0);
    
    private final int _phashCode;
    private final int _hashLength;
    private final int _saltMaxLength;
    
    private PasswordHashType(int phashCode,int hashLength,int saltMaxLength){
        _phashCode=phashCode;
        _hashLength=hashLength;
        _saltMaxLength=saltMaxLength;
    }
    
    public int getPasswordHashCode(){
        return _phashCode;
    }
    
    public int getHashLength(){
        return _hashLength;
    }
    
    public int getSaltMaxLength(){
        return _saltMaxLength;
    }
    
    public String encrypt(String password,String salt){
        switch(this){
            case DES:
                return (salt!=null && !salt.isEmpty())?UnixCrypt.crypt(password, salt):UnixCrypt.crypt(password);
            case MD5:
                return (salt!=null && !salt.isEmpty())?MyMd5Crypt.md5Crypt(password.getBytes(), "$1$"+salt):MyMd5Crypt.md5Crypt(password.getBytes(),null);
            case SHA256:
                return (salt!=null && !salt.isEmpty())?MySha2Crypt.sha256Crypt(password.getBytes(), "$5$"+salt):MySha2Crypt.sha256Crypt(password.getBytes(), null);
            case SHA512:
                return (salt!=null && !salt.isEmpty())?MySha2Crypt.sha512Crypt(password.getBytes(), "$6$"+salt):MySha2Crypt.sha512Crypt(password.getBytes(), null);
            default:
                return (salt!=null && !salt.isEmpty())?Crypt.crypt(password, salt):Crypt.crypt(password);
        }
    }
    
    public String extractSalt(String hashString){
        if(hashString==null || hashString.isEmpty()) return null;
        switch(this){
            case DES:
                return hashString.trim().substring(0, hashString.trim().length()-getHashLength());
            case MD5:
            case SHA256:
            case SHA512:
                String [] tmp;
                return ((tmp=hashString.trim().replaceFirst("^\\$", "").split("\\$")).length>2)?tmp[1]:null;
        }
        return null;
    }
    
    public String extractHash(String hashString){
        if(hashString==null || hashString.isEmpty()) return null;
        switch(this){
            case DES:
                return (hashString.trim().substring(hashString.trim().length()-getHashLength()));
            case MD5:
            case SHA256:
            case SHA512:
                String[] tmp;
                return ((tmp=hashString.trim().replaceFirst("^\\$", "").split("\\$")).length>2)?tmp[2]:null;
        }
        return null;
    }
    
    public static PasswordHashType createFromHashCode(int code){
        for(PasswordHashType pht:PasswordHashType.values()){
            if(pht.getPasswordHashCode()==code){
                return pht;
            }
        }
        return PasswordHashType.UNKNOWN_HASH_TYPE;
    }
    
    public static PasswordHashType createFromHashString(String hashString){
        if(hashString==null || hashString.isEmpty()) return null;
        String[] tmp=hashString.trim().replaceFirst("^\\$", "").split("\\$");
        if(tmp.length>2){
            return createFromHashCode(Integer.parseInt(tmp[0]));
        }
        else if((hashString.length()>=PasswordHashType.DES.getHashLength()) &&
                (hashString.length()<=(PasswordHashType.DES.getHashLength()+PasswordHashType.DES.getSaltMaxLength()))){
            return PasswordHashType.DES;
        }
        return PasswordHashType.UNKNOWN_HASH_TYPE;
    }
    
    public static boolean isCryptCompatibleHashString(String hashString){
        return ((hashString!=null) && ((hashString.trim().replaceFirst("^\\$", "").split("\\$").length>2) ||
                ((hashString.length()>=PasswordHashType.DES.getHashLength()) &&
                (hashString.length()<=(PasswordHashType.DES.getHashLength()+PasswordHashType.DES.getSaltMaxLength())))));
    }
    
    /**
     * Creates a PasswordTypeHash from a hash without its salt
     * @param hash
     * @return 
     */
    public static PasswordHashType createFromHash(String hash){
        for(PasswordHashType pht:PasswordHashType.values()){
            if(hash.length()==pht.getHashLength()){
                return pht;
            }
        }
        return PasswordHashType.UNKNOWN_HASH_TYPE;
    }
    
    
}
