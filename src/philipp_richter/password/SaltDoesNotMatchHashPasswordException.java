package philipp_richter.password;

/**
 *
 * @author Philipp Richter
 */
public class SaltDoesNotMatchHashPasswordException extends PasswordException{
    protected final String _salt;

    public SaltDoesNotMatchHashPasswordException(PasswordEntity pe,String salt) {
        super(pe,"Salt '"+salt+"' does not match hash '"+pe.getPasswordHash()+"' with password '"+pe.getPassword()+"'!");
        _salt=salt;
    }
    
    public String getSalt(){
        return _salt;
    }
}
