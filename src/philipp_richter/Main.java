
package philipp_richter;

import philipp_richter.server.Server;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import philipp_richter.client.Client;
import philipp_richter.gui.MainGUI;
import philipp_richter.server.CrackNodeResponseTypeFactory;

/**
 *
 * @author Philipp Richter
 */
public class Main {
    
    public static void main(String[] argv) throws SocketException, IOException, InterruptedException{
        final Server serv=new Server();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serv.stopListening();
                } catch (IOException ex) {
                    //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }));
        serv.listen();
        Client client=new Client(Client.CLIENT_CONNECTION_PORT, serv.getPort());
        MainGUI mainGUI=new MainGUI();
        Controller cont=new Controller(mainGUI,serv,client);
        
        mainGUI.setVisible(true);
    }
}
